################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../USER_CODE/Components/u8g2/u8g2_bitmap.c \
../USER_CODE/Components/u8g2/u8g2_box.c \
../USER_CODE/Components/u8g2/u8g2_buffer.c \
../USER_CODE/Components/u8g2/u8g2_button.c \
../USER_CODE/Components/u8g2/u8g2_circle.c \
../USER_CODE/Components/u8g2/u8g2_cleardisplay.c \
../USER_CODE/Components/u8g2/u8g2_d_memory.c \
../USER_CODE/Components/u8g2/u8g2_d_setup.c \
../USER_CODE/Components/u8g2/u8g2_font.c \
../USER_CODE/Components/u8g2/u8g2_fonts.c \
../USER_CODE/Components/u8g2/u8g2_hvline.c \
../USER_CODE/Components/u8g2/u8g2_input_value.c \
../USER_CODE/Components/u8g2/u8g2_intersection.c \
../USER_CODE/Components/u8g2/u8g2_kerning.c \
../USER_CODE/Components/u8g2/u8g2_line.c \
../USER_CODE/Components/u8g2/u8g2_ll_hvline.c \
../USER_CODE/Components/u8g2/u8g2_message.c \
../USER_CODE/Components/u8g2/u8g2_polygon.c \
../USER_CODE/Components/u8g2/u8g2_selection_list.c \
../USER_CODE/Components/u8g2/u8g2_setup.c \
../USER_CODE/Components/u8g2/u8log.c \
../USER_CODE/Components/u8g2/u8log_u8g2.c \
../USER_CODE/Components/u8g2/u8log_u8x8.c \
../USER_CODE/Components/u8g2/u8x8_8x8.c \
../USER_CODE/Components/u8g2/u8x8_byte.c \
../USER_CODE/Components/u8g2/u8x8_cad.c \
../USER_CODE/Components/u8g2/u8x8_capture.c \
../USER_CODE/Components/u8g2/u8x8_d_sh1107.c \
../USER_CODE/Components/u8g2/u8x8_d_ssd1306_128x64_noname.c \
../USER_CODE/Components/u8g2/u8x8_debounce.c \
../USER_CODE/Components/u8g2/u8x8_display.c \
../USER_CODE/Components/u8g2/u8x8_fonts.c \
../USER_CODE/Components/u8g2/u8x8_gpio.c \
../USER_CODE/Components/u8g2/u8x8_input_value.c \
../USER_CODE/Components/u8g2/u8x8_message.c \
../USER_CODE/Components/u8g2/u8x8_selection_list.c \
../USER_CODE/Components/u8g2/u8x8_setup.c \
../USER_CODE/Components/u8g2/u8x8_string.c \
../USER_CODE/Components/u8g2/u8x8_u16toa.c \
../USER_CODE/Components/u8g2/u8x8_u8toa.c 

OBJS += \
./USER_CODE/Components/u8g2/u8g2_bitmap.o \
./USER_CODE/Components/u8g2/u8g2_box.o \
./USER_CODE/Components/u8g2/u8g2_buffer.o \
./USER_CODE/Components/u8g2/u8g2_button.o \
./USER_CODE/Components/u8g2/u8g2_circle.o \
./USER_CODE/Components/u8g2/u8g2_cleardisplay.o \
./USER_CODE/Components/u8g2/u8g2_d_memory.o \
./USER_CODE/Components/u8g2/u8g2_d_setup.o \
./USER_CODE/Components/u8g2/u8g2_font.o \
./USER_CODE/Components/u8g2/u8g2_fonts.o \
./USER_CODE/Components/u8g2/u8g2_hvline.o \
./USER_CODE/Components/u8g2/u8g2_input_value.o \
./USER_CODE/Components/u8g2/u8g2_intersection.o \
./USER_CODE/Components/u8g2/u8g2_kerning.o \
./USER_CODE/Components/u8g2/u8g2_line.o \
./USER_CODE/Components/u8g2/u8g2_ll_hvline.o \
./USER_CODE/Components/u8g2/u8g2_message.o \
./USER_CODE/Components/u8g2/u8g2_polygon.o \
./USER_CODE/Components/u8g2/u8g2_selection_list.o \
./USER_CODE/Components/u8g2/u8g2_setup.o \
./USER_CODE/Components/u8g2/u8log.o \
./USER_CODE/Components/u8g2/u8log_u8g2.o \
./USER_CODE/Components/u8g2/u8log_u8x8.o \
./USER_CODE/Components/u8g2/u8x8_8x8.o \
./USER_CODE/Components/u8g2/u8x8_byte.o \
./USER_CODE/Components/u8g2/u8x8_cad.o \
./USER_CODE/Components/u8g2/u8x8_capture.o \
./USER_CODE/Components/u8g2/u8x8_d_sh1107.o \
./USER_CODE/Components/u8g2/u8x8_d_ssd1306_128x64_noname.o \
./USER_CODE/Components/u8g2/u8x8_debounce.o \
./USER_CODE/Components/u8g2/u8x8_display.o \
./USER_CODE/Components/u8g2/u8x8_fonts.o \
./USER_CODE/Components/u8g2/u8x8_gpio.o \
./USER_CODE/Components/u8g2/u8x8_input_value.o \
./USER_CODE/Components/u8g2/u8x8_message.o \
./USER_CODE/Components/u8g2/u8x8_selection_list.o \
./USER_CODE/Components/u8g2/u8x8_setup.o \
./USER_CODE/Components/u8g2/u8x8_string.o \
./USER_CODE/Components/u8g2/u8x8_u16toa.o \
./USER_CODE/Components/u8g2/u8x8_u8toa.o 

C_DEPS += \
./USER_CODE/Components/u8g2/u8g2_bitmap.d \
./USER_CODE/Components/u8g2/u8g2_box.d \
./USER_CODE/Components/u8g2/u8g2_buffer.d \
./USER_CODE/Components/u8g2/u8g2_button.d \
./USER_CODE/Components/u8g2/u8g2_circle.d \
./USER_CODE/Components/u8g2/u8g2_cleardisplay.d \
./USER_CODE/Components/u8g2/u8g2_d_memory.d \
./USER_CODE/Components/u8g2/u8g2_d_setup.d \
./USER_CODE/Components/u8g2/u8g2_font.d \
./USER_CODE/Components/u8g2/u8g2_fonts.d \
./USER_CODE/Components/u8g2/u8g2_hvline.d \
./USER_CODE/Components/u8g2/u8g2_input_value.d \
./USER_CODE/Components/u8g2/u8g2_intersection.d \
./USER_CODE/Components/u8g2/u8g2_kerning.d \
./USER_CODE/Components/u8g2/u8g2_line.d \
./USER_CODE/Components/u8g2/u8g2_ll_hvline.d \
./USER_CODE/Components/u8g2/u8g2_message.d \
./USER_CODE/Components/u8g2/u8g2_polygon.d \
./USER_CODE/Components/u8g2/u8g2_selection_list.d \
./USER_CODE/Components/u8g2/u8g2_setup.d \
./USER_CODE/Components/u8g2/u8log.d \
./USER_CODE/Components/u8g2/u8log_u8g2.d \
./USER_CODE/Components/u8g2/u8log_u8x8.d \
./USER_CODE/Components/u8g2/u8x8_8x8.d \
./USER_CODE/Components/u8g2/u8x8_byte.d \
./USER_CODE/Components/u8g2/u8x8_cad.d \
./USER_CODE/Components/u8g2/u8x8_capture.d \
./USER_CODE/Components/u8g2/u8x8_d_sh1107.d \
./USER_CODE/Components/u8g2/u8x8_d_ssd1306_128x64_noname.d \
./USER_CODE/Components/u8g2/u8x8_debounce.d \
./USER_CODE/Components/u8g2/u8x8_display.d \
./USER_CODE/Components/u8g2/u8x8_fonts.d \
./USER_CODE/Components/u8g2/u8x8_gpio.d \
./USER_CODE/Components/u8g2/u8x8_input_value.d \
./USER_CODE/Components/u8g2/u8x8_message.d \
./USER_CODE/Components/u8g2/u8x8_selection_list.d \
./USER_CODE/Components/u8g2/u8x8_setup.d \
./USER_CODE/Components/u8g2/u8x8_string.d \
./USER_CODE/Components/u8g2/u8x8_u16toa.d \
./USER_CODE/Components/u8g2/u8x8_u8toa.d 


# Each subdirectory must supply rules for building sources it contributes
USER_CODE/Components/u8g2/%.o: ../USER_CODE/Components/u8g2/%.c
	@	@	riscv-none-embed-gcc -march=rv32imc -mabi=ilp32 -mtune=size -msmall-data-limit=8 -mstrict-align -mno-save-restore -mno-div -mbranch-cost=1 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin  -g3 -I"D:\Project\MC3172\MC3172_Template\MC3172" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Example\oled" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\u8g2" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Example\bh1750" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Example\dht11" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Example\w5500" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\include" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\uart" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\delay" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\gpio" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\i2c" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\printf" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\pwm" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\spi" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@

