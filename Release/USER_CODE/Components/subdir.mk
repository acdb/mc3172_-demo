################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../USER_CODE/Components/stdlib.c \
../USER_CODE/Components/tools.c 

OBJS += \
./USER_CODE/Components/stdlib.o \
./USER_CODE/Components/tools.o 

C_DEPS += \
./USER_CODE/Components/stdlib.d \
./USER_CODE/Components/tools.d 


# Each subdirectory must supply rules for building sources it contributes
USER_CODE/Components/%.o: ../USER_CODE/Components/%.c
	@	@	riscv-none-embed-gcc -march=rv32imc -mabi=ilp32 -mtune=size -msmall-data-limit=8 -mstrict-align -mno-save-restore -mno-div -mbranch-cost=1 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -ffreestanding -fno-builtin  -g3 -I"D:\Project\MC3172\MC3172_Template\MC3172" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Example\oled" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\u8g2" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Example\bh1750" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Example\dht11" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Example\w5500" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\include" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\uart" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\delay" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\gpio" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\i2c" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\printf" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\pwm" -I"D:\Project\MC3172\MC3172_Template\USER_CODE\Components\spi" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@

