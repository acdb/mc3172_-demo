<!--
 * @Author: Memory 1619005172@qq.com
 * @Date: 2022-07-26 20:12:49
 * @LastEditors: Memory 1619005172@qq.com
 * @LastEditTime: 2022-08-24 20:38:36
 * @FilePath: 
 * @Description: 
-->
# MC3172_Demo

#### 介绍
Components目录下为基础驱动

Example目录下为测试例程

添加U8g2测试例程，软件I2C驱动SSD1306 0.96 OLED屏幕

不断更新中...

#### 安装教程



#### 使用说明

测试工程采用外部无源晶振（200M）

选用其他频率晶振需要修改延时函数中宏内核计数器频率（宏内核主频的1/4）

外部无源晶振为48M

内部高速RC振荡器频率为无源晶振4倍频（192M）精度1%

