#include "MC3172.h"
#include "delay.h"
#include "i2c.h"
#include "bh1750.h"

I2C_InitTypeDef BH1750_Int;

void BH1750_Init(void)
{
    BH1750_Int.SDA_Port=GPIOC_BASE_ADDR;
    BH1750_Int.SDA_Pin=GPIO_PIN1;
    BH1750_Int.SCL_Port=GPIOC_BASE_ADDR;
    BH1750_Int.SCL_Pin=GPIO_PIN0;
    i2c_init(&BH1750_Int);
}

unsigned int ReadLight(void)            //读取光照值
    {
     volatile u8 byte_1th;
     volatile u8 byte_2th;
     unsigned int sum;

     i2c_start(&BH1750_Int);

     i2c_send_byte(&BH1750_Int,LIGHTAddWr);
     i2c_wait_ack(&BH1750_Int);
     i2c_send_byte(&BH1750_Int,0x10);        //发送测量指令0x10(分辨率1lx),0x11(分辨率0.5lx)
     i2c_wait_ack(&BH1750_Int);
     i2c_stop(&BH1750_Int);

     delay_us(180000);
     i2c_start(&BH1750_Int);
     i2c_send_byte(&BH1750_Int,LIGHTAddRd);
     i2c_wait_ack(&BH1750_Int);
     byte_1th = i2c_read_byte(&BH1750_Int,1);    //读取高位数据

     byte_2th = i2c_read_byte(&BH1750_Int,0);    //读取低位数据

     i2c_stop(&BH1750_Int);

     sum = (byte_1th<<8)|byte_2th;   //数据合成

     return sum;                     //返回数据

    }

/*---------------------------------------------------------
    函数名称：Get_Light(void)
    函数作用：获取光照值
    参数：
    版本：Ver 0.1
    时间：2021/08/25
---------------------------------------------------------*/
unsigned int Get_Light(void)
{
    unsigned int light;
    light=ReadLight()/1.2;
    return light;
}
