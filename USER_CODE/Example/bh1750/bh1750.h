#ifndef USER_CODE_EXAMPLE_BH1750_BH1750_H_
#define USER_CODE_EXAMPLE_BH1750_BH1750_H_

#define LIGHTAddWr 0x46 //写数据地址
#define LIGHTAddRd 0x47 //读数据地址

void BH1750_Init(void);
unsigned int ReadLight(void);   //读取光照值函数
unsigned int Get_Light(void);

#endif /* USER_CODE_EXAMPLE_BH1750_BH1750_H_ */
