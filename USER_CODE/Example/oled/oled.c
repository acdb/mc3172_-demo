#include "oled.h"
#include "MC3172.h"
#include "i2c.h"
#include "delay.h"
#include "printf.h"
#include "../../Components/u8g2/u8g2.h"
#include "../../Components/u8g2/u8x8.h"

I2C_InitTypeDef OLED_init;
//初始化i2c引脚
void OLED_i2c_init(void){
    OLED_init.SDA_Port=GPIOC_BASE_ADDR;
    OLED_init.SDA_Pin=GPIO_PIN1;
    OLED_init.SCL_Port=GPIOC_BASE_ADDR;
    OLED_init.SCL_Pin=GPIO_PIN0;
    i2c_init(&OLED_init);
}

/*------------------------------------------------------------------------------------------------------------*/

void HW_I2cWrite(uint8_t *buf,uint8_t len)
{
    if(len<=0)
        return ;
    i2c_start(&OLED_init);
    i2c_send_byte(&OLED_init, OLED_ADDRESS);
    i2c_wait_ack(&OLED_init);
    for(uint8_t i=0;i<len;i++)
    {
        i2c_send_byte(&OLED_init, buf[i]);
        i2c_wait_ack(&OLED_init);
    }
    i2c_stop(&OLED_init);
}




uint8_t u8x8_byte_mc3172_hw_i2c(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
    static uint8_t buffer[32];      /* u8g2/u8x8 will never send more than 32 bytes between START_TRANSFER and END_TRANSFER */
    static uint8_t buf_idx;
    uint8_t *data;

    switch(msg)
    {
        case U8X8_MSG_BYTE_SEND:
          data = (uint8_t *)arg_ptr;
          while( arg_int > 0 ){
                    buffer[buf_idx++] = *data;
                    data++;
                    arg_int--;
                }
        break;

        case U8X8_MSG_BYTE_INIT:
          /* add your custom code to init i2c subsystem */
        break;

        case U8X8_MSG_BYTE_START_TRANSFER:
          buf_idx = 0;
        break;

        case U8X8_MSG_BYTE_END_TRANSFER:
            HW_I2cWrite(buffer,buf_idx);
        break;

        default:
          return 0;
    }
    return 1;
}


uint8_t u8g2_gpio_and_delay_mc3172(U8X8_UNUSED u8x8_t *u8x8, U8X8_UNUSED uint8_t msg, U8X8_UNUSED uint8_t arg_int, U8X8_UNUSED void *arg_ptr)
{
    switch(msg)
    {
        case U8X8_MSG_GPIO_AND_DELAY_INIT:
            break;

        case U8X8_MSG_DELAY_MILLI:
//            delay_ms(arg_int);
            break;

        case U8X8_MSG_GPIO_I2C_CLOCK:
            break;

        case U8X8_MSG_GPIO_I2C_DATA:
            break;

        default:
            return 0;
    }
    return 1; // command processed successfully.
}



static u8g2_t u8g2;
void U8g2Init(void)
{

    OLED_i2c_init();
    u8g2_Setup_ssd1306_i2c_128x64_noname_f(&u8g2,U8G2_R0,u8x8_byte_mc3172_hw_i2c,u8g2_gpio_and_delay_mc3172);

    u8g2_InitDisplay(&u8g2); // send init sequence to the display, display is in sleep mode after this,
    u8g2_SetPowerSave(&u8g2, 0); // wake up display
    u8g2_SetFont(&u8g2,u8g2_font_10x20_mf);//设置英文字体
//    u8g2_SetFontMode(u8g2, 1); /*字体模式选择*/
//    u8g2_SetFontDirection(u8g2, 0); /*字体方向选择*/
//    u8g2_SetFont(u8g2, u8g2_font_inb24_mf); /*字库选择*/
}


void draw(u8g2_t *u8g2)
{
    u8g2_SetFontMode(u8g2, 1); /*字体模式选择*/
    u8g2_SetFontDirection(u8g2, 0); /*字体方向选择*/
    u8g2_SetFont(u8g2, u8g2_font_inb24_mf); /*字库选择*/
    u8g2_DrawStr(u8g2, 0, 20, "U");

    u8g2_SetFontDirection(u8g2, 1);
    u8g2_SetFont(u8g2, u8g2_font_inb30_mn);
    u8g2_DrawStr(u8g2, 21,8,"8");

    u8g2_SetFontDirection(u8g2, 0);
    u8g2_SetFont(u8g2, u8g2_font_inb24_mf);
    u8g2_DrawStr(u8g2, 51,30,"g");
    u8g2_DrawStr(u8g2, 67,30,"\xb2");

    u8g2_DrawHLine(u8g2, 2, 35, 47);
    u8g2_DrawHLine(u8g2, 3, 36, 47);
    u8g2_DrawVLine(u8g2, 45, 32, 12);
    u8g2_DrawVLine(u8g2, 46, 33, 12);

    u8g2_SetFont(u8g2, u8g2_font_4x6_tr);
    u8g2_DrawStr(u8g2, 1,54,"github.com/olikraus/u8g2");
}



void U8g2Test(void)
{
    u32 i = 0;
    u8 str[20000] = {0};
    while(1){
        sprintf(str,"%d",i);
        u8g2_DrawStr(&u8g2,0,40,str);
        u8g2_SendBuffer(&u8g2);
        i++;
//        if (i>10000) {
//            i = 0;
//        }
    }
//    u8g2_FirstPage(&u8g2);
//    do{
//        draw(&u8g2);
//    } while (u8g2_NextPage(&u8g2));
}
/*-----------------------------------------------------------------------------------------*/

