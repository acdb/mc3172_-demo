#ifndef __OLED_H
#define __OLED_H

#include "MC3172.h"

#define USE_HORIZONTAL 0

#define OLED_ADDRESS 0x78   //7位地址
////设备地址
//
#define OLED_WriteCom_Addr  0x00    //从机写指令地址
#define OLED_WriteData_Addr 0x40    //从机写数据地址
//
#define OLED_CMD  0 //写命令
#define OLED_DATA 1 //写数据

void OLED_i2c_init(void);


void U8g2Init(void);
void U8g2Test(void);


#endif  /* __OLED_H */

