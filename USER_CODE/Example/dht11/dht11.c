/*
 * dht11.c
 *
 *  Created on: 2022年7月13日
 *      Author: Memory
 */
#include "MC3172.h"
#include "delay.h"
#include "gpio.h"
#include "printf.h"
#include "dht11.h"

#define GPIO_MODE_OUT 1
#define GPIO_MODE_IN  0


void dht11_delay(u16 nus){
    delay_us(nus);
}

//初始化GPIO为输出模式
static void DHT11_init(u32 gpio_sel,u32 gpio_pin){
    INTDEV_SET_CLK_RST(gpio_sel,(INTDEV_RUN|INTDEV_IS_GROUP0|INTDEV_CLK_IS_CORECLK_DIV2));
    GPIO_SET_OUTPUT_EN_VALUE(gpio_sel,gpio_pin,GPIO_SET_ENABLE);
    GPIO_SET_OUTPUT_PIN_TO_1(gpio_sel,gpio_pin);//未开始工作，引脚初始化为高电平
}

//DHT11读取一位数据
static u8 DHT11_read_bit(u32 gpio_sel,u32 gpio_pin){
    while(PGin(gpio_sel,gpio_pin));//等待变为低电平
    while(!PGin(gpio_sel,gpio_pin));//等待变为高电平
    dht11_delay(40);
    return PGin(gpio_sel,gpio_pin);
}
//DHT11读取一个字节
static u8 DHT11_read_byte(u32 gpio_sel,u32 gpio_pin){
    u8 var,data = 0;
    for (var = 0; var < 8; var++) {
        data <<= 1;
        data |= DHT11_read_bit(gpio_sel,gpio_pin);
    }
    return data;
}
//DHT11测试
u8 DHT11_read_data(u32 gpio_sel,u32 gpio_pin,u16 *temp,u16 *humi){
    u8 buf[5];
    u32 t = 0;
    DHT11_init(gpio_sel,gpio_pin);
    dht11_delay(10000);
    //主机发送开始信号
    GPIO_SET_OUTPUT_PIN_TO_0(gpio_sel,gpio_pin);
    dht11_delay(20000);
    GPIO_SET_OUTPUT_PIN_TO_1(gpio_sel,gpio_pin);
    dht11_delay(30);
    //切换GPIO为输入模式
    gpio_pin_mode(gpio_sel,gpio_pin,GPIO_PIN_IN);
    //读取DHT11响应信号
    while(PGin(gpio_sel,gpio_pin) && t < 50){//等待相应
        t++;
        dht11_delay(2);
    }
    if (t >= 100) {//响应超时
        printf("响应超时\n");
        gpio_pin_mode(gpio_sel,gpio_pin,GPIO_MODE_OUT);
        GPIO_SET_OUTPUT_PIN_TO_1(gpio_sel,gpio_pin);
        return 0;
    }
    while (!PGin(gpio_sel,gpio_pin)&&t<100)//DHT11拉低后会再次拉高40~80us
    {
        t++;
        dht11_delay(1);
    }
    if (t >= 100) {//响应超时
        printf("响应超时\n");
        gpio_pin_mode(gpio_sel,gpio_pin,GPIO_MODE_OUT);
        GPIO_SET_OUTPUT_PIN_TO_1(gpio_sel,gpio_pin);
        return 0;
    }
    //读取数据
    for (u8 var = 0; var < 5; var++) {
        buf[var] = DHT11_read_byte(gpio_sel,gpio_pin);
    }
    if (buf[0]+buf[1]+buf[2]+buf[3] == buf[4]) {//数据校验
        *temp = buf[2];//温度整数位
        *humi = buf[0];//湿度整数位
    }else {
        printf("数据校验失败\n");
        gpio_pin_mode(gpio_sel,gpio_pin,GPIO_MODE_OUT);
        GPIO_SET_OUTPUT_PIN_TO_1(gpio_sel,gpio_pin);
        return 0;//数据校验失败
    }
    gpio_pin_mode(gpio_sel,gpio_pin,GPIO_MODE_OUT);
    GPIO_SET_OUTPUT_PIN_TO_1(gpio_sel,gpio_pin);
    return t;
}
