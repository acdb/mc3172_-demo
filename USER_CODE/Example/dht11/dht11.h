/*
 * dht11.h
 *
 *  Created on: 2022��7��13��
 *      Author: Memory
 */

#ifndef USER_CODE_EXAMPLE_DHT11_DHT11_H_
#define USER_CODE_EXAMPLE_DHT11_DHT11_H_
//void DHT11_pin_mode(u32 gpio_sel,u32 gpio_pin,u8 mode);
u8 DHT11_read_data(u32 gpio_sel,u32 gpio_pin,u16 *temp,u16 *humi);
#endif /* USER_CODE_EXAMPLE_DHT11_DHT11_H_ */
