/*
 * w5500test.h
 *
 *  Created on: 2022��8��12��
 *      Author: w1619
 */

#ifndef USER_CODE_EXAMPLE_W5500_W5500TEST_H_
#define USER_CODE_EXAMPLE_W5500_W5500TEST_H_

void System_Initialization(void);
void Load_Net_Parameters(void);
void W5500_Initialization(void);
void W5500_Socket_Set(void);
void Process_Socket_Data(SOCKET s);

#endif /* USER_CODE_EXAMPLE_W5500_W5500TEST_H_ */
