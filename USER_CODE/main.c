/*--------------------------------------------------------------------------
main.c
****************************************
**  Copyright  (C)    2021-2022   **
**  Web:              http://rothd.cn   **
****************************************
--------------------------------------------------------------------------*/

#include "MC3172.h"
#include "delay.h"
#include "example.h"
#include "tools.h"
#include "printf.h"
#include "spi.h"
#include "Example/dht11/dht11.h"
#include "Example/bh1750/bh1750.h"
#include "Example/w5500/w5500.h"
#include "Example/w5500/w5500test.h"
#include "Example/oled/oled.h"
#include <string.h>
#include "u8g2.h"


////////////////////////////////////////////////////////////


void thread_end(void)
{
    while(1);
}

////////////////////////////////////////////////////////////
//DHT11测试线程
void thread0_main(void)
{
//    u16 temp,humi;
//    printf("线程0\n");
    while(1){
//        DHT11_read_data(GPIOD_BASE_ADDR,GPIO_PIN6,&temp,&humi);
        delay_ms(1000);
        printf("test1\n");
//        printf("线程0----温度%d 湿度%d\n",temp,humi);
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread1_main(void)
{
    DEBUG_LOG_init();
//    SPI_IoInitTypeDef io = {
//            .SPI_Port = GPCOM0_BASE_ADDR,
//            .SPI_CLK = SPI_P3,
//            .SPI_CS = SPI_P0,
//            .SPI_MISO = SPI_P2,
//            .SPI_MOSI = SPI_P1
//    };
//    SPI_InitTypeDef cfg = {
//            .SPI_IoSet = &io,
//            .SPI_Mode = GPCOM_SPI_MASTER_MODE3,
//            .SPI_TxFirstBit = GPCOM_TX_MSB_FIRST,
//            .SPI_RxFirstBit = GPCOM_RX_MSB_FIRST,
//            .SPI_SPEED = 1000000
//    };
    while(1){
//        GPCOM_SPI(&cfg);
        printf("test2\n");
        delay_ms(1500);
    }
    thread_end();
}

////////////////////////////////////////////////////////////
//W5500测试线程
void thread2_main(void)
{
//    System_Initialization();    //初始化系统参数
//    Load_Net_Parameters();      //装载网络参数
//    W5500_Hardware_Reset();     //硬件复位W5500
//    W5500_Initialization();     //W5500初始化配置
    while(1){
//        W5500_Socket_Set();     //W5500端口初始化配置
//        W5500_Interrupt_Process();//W5500中断处理程序框架
//        if((S0_Data & S_RECEIVE) == S_RECEIVE)//如果Socket0接收到数据
//        {
//            S0_Data&=~S_RECEIVE;
//            Process_Socket_Data(0);//W5500接收并发送接收到的数据
//        }
    }
    thread_end();
}

////////////////////////////////////////////////////////////
//BH1750测试线程
void thread3_main(void)
{
//    u16 light;
//    BH1750_Init();
    while(1){
//        printf("test\n");
//        delay_ms(1000);
//        light = Get_Light();
//        printf_("线程3----光强：%dlx\n",light);
//        delay_ms(1000);
    }
    thread_end();
}

////////////////////////////////////////////////////////////
//I2C OLED测试线程
void thread4_main(void)
{
    U8g2Init();
    while(1){
        U8g2Test();
        printf("OLED_TEST\n");
        delay_ms(2000);
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread5_main(void)
{
    while(1){
        printf("thread5");
        delay_ms(1000);
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread6_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread7_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread8_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread9_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread10_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread11_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread12_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread13_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread14_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread15_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread16_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread17_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread18_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread19_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread20_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread21_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread22_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread23_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread24_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread25_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread26_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread27_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread28_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread29_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread30_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread31_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread32_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread33_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread34_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread35_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread36_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread37_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread38_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread39_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread40_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread41_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread42_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread43_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread44_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread45_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread46_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread47_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread48_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread49_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread50_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread51_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread52_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread53_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread54_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread55_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread56_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread57_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread58_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread59_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread60_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread61_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread62_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////

void thread63_main(void)
{
    while(1){
        //user code section
    }
    thread_end();
}

////////////////////////////////////////////////////////////





