/*
 * i2c.c
 *
 *  Created on: 2022年7月24日
 *      Author: Memory
 */

#include "i2c.h"
#include "delay.h"
#include "gpio.h"



void SDA_OUT(I2C_InitTypeDef *I2C){
//    gpio_pin_mode(I2C->SDA_Port,I2C->SDA_Pin,1);
    GPIO_SET_INPUT_EN_VALUE(I2C->SDA_Port,I2C->SDA_Pin,GPIO_SET_DISABLE);
    GPIO_SET_OUTPUT_EN_VALUE(I2C->SDA_Port,I2C->SDA_Pin,GPIO_SET_ENABLE);
}
void SDA_IN(I2C_InitTypeDef *I2C){
//    gpio_pin_mode(I2C->SDA_Port,I2C->SDA_Pin,0);
    GPIO_SET_OUTPUT_EN_VALUE(I2C->SDA_Port,I2C->SDA_Pin,GPIO_SET_DISABLE);
    GPIO_SET_INPUT_EN_VALUE(I2C->SDA_Port,I2C->SDA_Pin,GPIO_SET_ENABLE);
}
void SDA_H(I2C_InitTypeDef *I2C){
    GPIO_SET_OUTPUT_PIN_TO_1(I2C->SDA_Port,I2C->SDA_Pin);
}
void SDA_L(I2C_InitTypeDef *I2C){
    GPIO_SET_OUTPUT_PIN_TO_0(I2C->SDA_Port,I2C->SDA_Pin);
}
void SCL_H(I2C_InitTypeDef *I2C){
    GPIO_SET_OUTPUT_PIN_TO_1(I2C->SCL_Port,I2C->SCL_Pin);
}
void SCL_L(I2C_InitTypeDef *I2C){
    GPIO_SET_OUTPUT_PIN_TO_0(I2C->SCL_Port,I2C->SCL_Pin);
}

void i2c_delay(){
    delay_us(1);//5us延时
}

//i2c引脚初始化
void i2c_init(I2C_InitTypeDef *I2C){
    INTDEV_SET_CLK_RST(I2C->SCL_Port,(INTDEV_RUN|INTDEV_IS_GROUP0|INTDEV_CLK_IS_CORECLK_DIV4));
    INTDEV_SET_CLK_RST(I2C->SDA_Port,(INTDEV_RUN|INTDEV_IS_GROUP0|INTDEV_CLK_IS_CORECLK_DIV4));
    GPIO_SET_OUTPUT_EN_VALUE(I2C->SCL_Port,I2C->SCL_Pin,GPIO_SET_ENABLE);
    GPIO_SET_OUTPUT_EN_VALUE(I2C->SDA_Port,I2C->SDA_Pin,GPIO_SET_ENABLE);
    GPIO_SET_OUTPUT_PIN_TO_1(I2C->SDA_Port,I2C->SDA_Pin);
    GPIO_SET_OUTPUT_PIN_TO_1(I2C->SCL_Port,I2C->SCL_Pin);
}

//产生i2c起始信号
void i2c_start(I2C_InitTypeDef *I2C)
{
    SDA_H(I2C);
    i2c_delay();
    SCL_H(I2C);
    i2c_delay();
    SDA_L(I2C);
    i2c_delay();
    SCL_L(I2C);
#ifdef I2C_FAST
    i2c_delay();
#endif
}

//产生i2c停止信号
void i2c_stop(I2C_InitTypeDef *I2C)
{
    SDA_L(I2C);
    i2c_delay();
    SCL_H(I2C);
    i2c_delay();
    SDA_H(I2C);
//    i2c_delay();
}

//读取ack
u8 i2c_wait_ack(I2C_InitTypeDef *I2C)
{
    u8 i = 0;
    u8 count = 2;
    SDA_IN(I2C);
//    SDA_H(I2C);
//    i2c_delay();
    SCL_H(I2C);
    i2c_delay();
    do {
        count--;
        if(PGin(I2C->SDA_Port,I2C->SDA_Pin)){
            i = 1;
        }
        else{
            i = 0;
            break;
        }
        i2c_delay();
    } while (count>0);
    SCL_L(I2C);
#ifdef I2C_FAST
    i2c_delay();
#endif
    SDA_OUT(I2C);
    return i;
}

//产生ack应答
void i2c_ack(I2C_InitTypeDef *I2C)
{
    SDA_L(I2C);
    i2c_delay();
    SCL_H(I2C);
    i2c_delay();
    SCL_L(I2C);
#ifdef I2C_FAST
    i2c_delay();
#endif
    SDA_H(I2C);
}

//不产生ack应答
void i2c_nack(I2C_InitTypeDef *I2C)
{
    SDA_H(I2C);
    i2c_delay();
    SCL_H(I2C);
    i2c_delay();
    SCL_L(I2C);
#ifdef I2C_FAST
    i2c_delay();
#endif
}

//i2c发送一个字节
void i2c_send_byte(I2C_InitTypeDef *I2C,u8 txd)
{
    u8 t;
    for(t=0; t<8; t++)
    {
        if(txd & 0x80)
            SDA_H(I2C);
        else
            SDA_L(I2C);
        txd <<= 1;
        i2c_delay();
        SCL_H(I2C);
        i2c_delay();
        SCL_L(I2C);
#ifdef I2C_FAST
        i2c_delay();
#endif
    }
#ifdef I2C_FAST
    SDA_H(I2C);
    i2c_delay();
#endif
}

//读1个字节
u8 i2c_read_byte(I2C_InitTypeDef *I2C,u8 ack)
{
    u8 i,receive=0;
    SDA_IN(I2C);
    for(i=0; i<8; i++ )
    {
        SCL_H(I2C);
        i2c_delay();
        receive <<= 1;
        if(PGin(I2C->SDA_Port,I2C->SDA_Pin))
            receive++;
        SCL_L(I2C);
        i2c_delay();
    }
    SDA_OUT(I2C);
    if (!ack)
        i2c_nack(I2C);
    else
        i2c_ack(I2C);
    return receive;
}
