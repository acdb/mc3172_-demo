/*
 * i2c.h
 *
 *  Created on: 2022年7月24日
 *      Author: Memory
 */

#ifndef USER_CODE_COMPONENTS_I2C_I2C_H_
#define USER_CODE_COMPONENTS_I2C_I2C_H_
#include "MC3172.h"

#define I2C_FAST 0


typedef struct{
    u32 SDA_Port;   //SDA端口
    u32 SCL_Port;   //SCL端口
    u32 SDA_Pin;    //SDA PIN
    u32 SCL_Pin;    //SCL PIN
    u32 delay_us;   //i2c延时(一般1-10us)
}I2C_InitTypeDef;


void i2c_delay();
void i2c_init(I2C_InitTypeDef *I2C);
void i2c_start(I2C_InitTypeDef *I2C);
void i2c_stop(I2C_InitTypeDef *I2C);
u8 i2c_wait_ack(I2C_InitTypeDef *I2C);
void i2c_ack(I2C_InitTypeDef *I2C);
void i2c_nack(I2C_InitTypeDef *I2C);
void i2c_send_byte(I2C_InitTypeDef *I2C,u8 txd);
u8 i2c_read_byte(I2C_InitTypeDef *I2C,u8 ack);

#endif /* USER_CODE_COMPONENTS_I2C_I2C_H_ */
