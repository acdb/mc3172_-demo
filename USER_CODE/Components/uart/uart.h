/*
 * usart.h
 *
 *  Created on: 2022年7月29日
 *      Author: w1619
 */

#ifndef USER_CODE_COMPONENTS_UART_UART_H_
#define USER_CODE_COMPONENTS_UART_UART_H_
#include "MC3172.h"
struct serial_configure
{
    u32 baud_rate;            /* 波特率 */
    u32 data_bits    :4;      /* 数据位 */
    u32 stop_bits    :2;      /* 停止位 */
    u32 parity       :2;      /* 奇偶校验位 */
    u32 bit_order    :1;      /* 高位在前或者低位在前 */
    u32 invert       :1;      /* 模式 */
    u32 bufsz        :16;     /* 接收数据缓冲区大小 */
    u32 reserved     :4;      /* 保留位 */
};


/* 波特率可取值 */
#define BAUD_RATE_2400                  2400
#define BAUD_RATE_4800                  4800
#define BAUD_RATE_9600                  9600
#define BAUD_RATE_19200                 19200
#define BAUD_RATE_38400                 38400
#define BAUD_RATE_57600                 57600
#define BAUD_RATE_115200                115200
#define BAUD_RATE_230400                230400
#define BAUD_RATE_460800                460800
#define BAUD_RATE_921600                921600
#define BAUD_RATE_2000000               2000000
#define BAUD_RATE_3000000               3000000
/* 数据位可取值 */
#define DATA_BITS_5                     5
#define DATA_BITS_6                     6
#define DATA_BITS_7                     7
#define DATA_BITS_8                     8
#define DATA_BITS_9                     9
/* 停止位可取值 */
#define STOP_BITS_1                     0
#define STOP_BITS_2                     1
#define STOP_BITS_3                     2
#define STOP_BITS_4                     3
/* 极性位可取值 */
#define PARITY_NONE                     0
#define PARITY_ODD                      1
#define PARITY_EVEN                     2
/* 高低位顺序可取值 */
#define BIT_ORDER_LSB                   0
#define BIT_ORDER_MSB                   1
/* 模式可取值 */
#define NRZ_NORMAL                      0     /* normal mode */
#define NRZ_INVERTED                    1     /* inverted mode */
/* 接收数据缓冲区默认大小 */
#define SERIAL_RB_BUFSZ              64


#define SERIAL_CONFIG_DEFAULT           \
{                                          \
    BAUD_RATE_115200, /* 115200 bits/s */  \
    DATA_BITS_8,      /* 8 databits */     \
    STOP_BITS_1,      /* 1 stopbit */      \
    PARITY_NONE,      /* No parity  */     \
    BIT_ORDER_LSB,    /* LSB first sent */ \
    NRZ_NORMAL,       /* Normal mode */    \
    RT_SERIAL_RB_BUFSZ, /* Buffer size */  \
    0                                      \
}


#endif /* USER_CODE_COMPONENTS_UART_UART_H_ */
