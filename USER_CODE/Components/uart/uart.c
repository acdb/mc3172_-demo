/*
 * usart.c
 *
 *  Created on: 2022��7��29��
 *      Author: w1619
 */

#include "uart.h"

#include "MC3172.h"

#include "printf.h"



void _putchar(char character){
    while(GPCOM_TX_FIFO_FULL(GPCOM4_BASE_ADDR));
    GPCOM_PUSH_TX_DATA(GPCOM4_BASE_ADDR,character);
}
