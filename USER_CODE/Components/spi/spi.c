/*
 * spi.c
 *
 *  Created on: 2022年8月5日
 *      Author: Memory
 */
#include "MC3172.h"
#include "delay.h"
#include "spi.h"

//初始化SPI引脚
void spi_io_init(SPI_IoInitTypeDef *iocfg){
    INTDEV_SET_CLK_RST(iocfg->SPI_Port,(INTDEV_RUN|INTDEV_IS_GROUP0|INTDEV_CLK_IS_CORECLK));
    //GPIO方式使能CS输出
    GPIO_SET_OUTPUT_EN_VALUE(iocfg->SPI_CS_PORT,iocfg->SPI_CS_PIN,GPIO_SET_ENABLE);
    //设置MISO
    GPCOM_SET_IN_PORT(iocfg->SPI_Port,(GPCOM_MASTER_IN_IS_P0+iocfg->SPI_MISO));
    //初始化SCK,MOSI,CS引脚
    GPCOM_SET_OUT_PORT(iocfg->SPI_Port,(                         \
                //MOSI使能输出
                (GPCOM_P0_OUTPUT_ENABLE<<(iocfg->SPI_MOSI*4))|   \
                //MISO失能输出
                (GPCOM_P0_OUTPUT_DISABLE<<(iocfg->SPI_MISO*4))|  \
                //选择MOSI
                (GPCOM_P0_IS_MASTER_OUT<<(iocfg->SPI_MOSI*4))|   \
                //拉高MISO
                (GPCOM_P0_IS_HIGH<<(iocfg->SPI_MISO*4))|         \
                //选择CLK
                (GPCOM_P2_IS_MASTER_CLK<<(iocfg->SPI_CLK*4))     \
                ));
}

//初始化SPI设置
void spi_init_config(SPI_InitTypeDef *cfg){
    //选择MISO及设置发送接收MBR
    GPCOM_SET_COM_MODE(cfg->SPI_IoSet->SPI_Port,(cfg->SPI_Mode|cfg->SPI_TxFirstBit|cfg->SPI_RxFirstBit));
    //设置SPI速率
    GPCOM_SET_COM_SPEED(cfg->SPI_IoSet->SPI_Port,50000000,cfg->SPI_SPEED);
    //管脚映射
    GPCOM_SET_OVERRIDE_GPIO(cfg->SPI_IoSet->SPI_Port, ( \
                (GPCOM_P1_OVERRIDE_GPIO)| \
                (GPCOM_P2_OVERRIDE_GPIO)| \
                (GPCOM_P3_OVERRIDE_GPIO)| \
                (GPCOM_P0_INPUT_ENABLE<<(8*cfg->SPI_IoSet->SPI_MISO))));

}

//CS控制
void spi_cs_ctl(SPI_IoInitTypeDef *iocfg,u8 i){
    if (i) {
        GPIO_SET_OUTPUT_PIN_TO_1(iocfg->SPI_CS_PORT,iocfg->SPI_CS_PIN);
    }
    else {
        GPIO_SET_OUTPUT_PIN_TO_0(iocfg->SPI_CS_PORT,iocfg->SPI_CS_PIN);
    }

}

//发送一字节数据
void spi_send_data(u32 gpcom_sel,u8 dat){
    u8 tx_data_wp=0;
    tx_data_wp=GPCOM_GET_TX_WP(gpcom_sel);
    GPCOM_SEND_TX_DATA(gpcom_sel,tx_data_wp+0,dat);
    tx_data_wp+=1;
    GPCOM_SEND_TX_WP(gpcom_sel,tx_data_wp);
    while(!(GPCOM_TX_FIFO_EMPTY(gpcom_sel)));
}

//接收一字节
u8 spi_recive_data(u32 gpcom_sel){
    u8 recive = 0;
    u8 rx_data_rp=0;
    rx_data_rp=GPCOM_GET_RX_WP(gpcom_sel);
    recive = GPCOM_GET_RX_DATA(gpcom_sel,rx_data_rp);
    return recive;
}


//测试函数
void GPCOM_SPI(SPI_InitTypeDef *cfg)
{
    volatile u8 rx_data_temp[8];

    spi_io_init(cfg->SPI_IoSet);
    spi_init_config(cfg);
//    INTDEV_SET_CLK_RST(gpcom_sel,(INTDEV_RUN|INTDEV_IS_GROUP0|INTDEV_CLK_IS_CORECLK_DIV2));
//
//    GPCOM_SET_IN_PORT(gpcom_sel,(GPCOM_MASTER_IN_IS_P2));
//    GPCOM_SET_OUT_PORT(gpcom_sel,( \
//            GPCOM_P0_OUTPUT_ENABLE|GPCOM_P1_OUTPUT_ENABLE|GPCOM_P2_OUTPUT_DISABLE|GPCOM_P3_OUTPUT_ENABLE| \
//            GPCOM_P0_IS_HIGH|      GPCOM_P1_IS_MASTER_OUT|GPCOM_P2_IS_HIGH|       GPCOM_P3_IS_MASTER_CLK
//                      ));
//
//    GPCOM_SET_COM_MODE(gpcom_sel,(GPCOM_SPI_MASTER_MODE0|GPCOM_TX_MSB_FIRST|GPCOM_RX_MSB_FIRST));
//
//    GPCOM_SET_COM_SPEED(gpcom_sel,24000000,1000000);
//
//    GPCOM_SET_OVERRIDE_GPIO(gpcom_sel, ( \
//            GPCOM_P0_OVERRIDE_GPIO| \
//            GPCOM_P1_OVERRIDE_GPIO| \
//            GPCOM_P2_OVERRIDE_GPIO| \
//            GPCOM_P3_OVERRIDE_GPIO|GPCOM_P2_INPUT_ENABLE  \
//                                              ));
//



    u8 tx_data_wp=0;
    u8 rx_data_rp=0;
    tx_data_wp=GPCOM_GET_TX_WP(cfg->SPI_IoSet->SPI_Port);
    rx_data_rp=GPCOM_GET_RX_WP(cfg->SPI_IoSet->SPI_Port);

    while(1){
        spi_cs_ctl(cfg->SPI_IoSet,0);
        GPCOM_SET_OUT_PORT(GPCOM0_BASE_ADDR,( \
                    GPCOM_P0_OUTPUT_ENABLE|GPCOM_P1_OUTPUT_ENABLE|GPCOM_P2_OUTPUT_DISABLE|GPCOM_P3_OUTPUT_ENABLE| \
                    GPCOM_P0_IS_LOW|      GPCOM_P1_IS_MASTER_OUT|GPCOM_P2_IS_HIGH|GPCOM_P3_IS_MASTER_CLK
                              ));

        GPCOM_SEND_TX_DATA(cfg->SPI_IoSet->SPI_Port,tx_data_wp+0,0x01);
        GPCOM_SEND_TX_DATA(cfg->SPI_IoSet->SPI_Port,tx_data_wp+1,0x02);
        GPCOM_SEND_TX_DATA(cfg->SPI_IoSet->SPI_Port,tx_data_wp+2,0x03);
        GPCOM_SEND_TX_DATA(cfg->SPI_IoSet->SPI_Port,tx_data_wp+3,0xff);

        tx_data_wp+=4;
        GPCOM_SEND_TX_WP(cfg->SPI_IoSet->SPI_Port,tx_data_wp);
        while(!(GPCOM_TX_FIFO_EMPTY(cfg->SPI_IoSet->SPI_Port))){};




//        while(rx_data_rp!=GPCOM_GET_RX_WP(cfg->SPI_IoSet->SPI_Port)){
//            rx_data_temp[rx_data_rp&0x7]=GPCOM_GET_RX_DATA(cfg->SPI_IoSet->SPI_Port,rx_data_rp);
//            rx_data_rp+=1;
//            rx_data_rp&=0xf;
//
//        };

        spi_cs_ctl(cfg->SPI_IoSet,1);
//        GPCOM_SET_OUT_PORT(GPCOM0_BASE_ADDR,( \
//                    GPCOM_P0_OUTPUT_ENABLE|GPCOM_P1_OUTPUT_ENABLE|GPCOM_P2_OUTPUT_DISABLE|GPCOM_P3_OUTPUT_ENABLE| \
//                    GPCOM_P0_IS_HIGH|      GPCOM_P1_IS_MASTER_OUT|GPCOM_P2_IS_HIGH|GPCOM_P3_IS_MASTER_CLK
//                              ));
    }

}
