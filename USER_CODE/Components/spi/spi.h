/*
 * spi.h
 *
 *  Created on: 2022年8月5日
 *      Author: Memory
 */

#ifndef _SPI_H_
#define _SPI_H_
#include "MC3172.h"

enum{
    SPI_P0 = 0,
    SPI_P1,
    SPI_P2,
    SPI_P3,
};

typedef struct{
    u8 SPI_MISO;               //MISO
    u8 SPI_MOSI;               //MOSI
    u8 SPI_CLK;                //CLK
    u8 SPI_CS;                 //CS
    u32 SPI_Port;               //GPCOM
    u32 SPI_CS_PORT;
    u32 SPI_CS_PIN;

}SPI_IoInitTypeDef;

typedef struct{
    u8 SPI_DataSize;            //数据宽度
    u8 SPI_Direction;           //单双向模式选择
    u8 SPI_CRCPolynomial;       //CRC校验
    u32 SPI_TxFirstBit;          //发送大小端选择
    u32 SPI_RxFirstBit;          //接收大小端选择
    u32 SPI_Mode;               //模式选择
    u32 SPI_SPEED;              //SPI速度
    SPI_IoInitTypeDef *SPI_IoSet;

}SPI_InitTypeDef;


void spi_send_data(u32 spi_Sel,u8 dat);
u8 spi_recive_data(u32 gpcom_sel);
void GPCOM_SPI(SPI_InitTypeDef *cfg);

#endif /* USER_CODE_COMPONENTS_SPI_SPI_H_ */
