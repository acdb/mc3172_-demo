/*
 * delay.h
 *
 *  Created on: 2022年7月26日
 *      Author: w1619
 */

#ifndef USER_CODE_COMPONENTS_DELAY_DELAY_H_
#define USER_CODE_COMPONENTS_DELAY_DELAY_H_
#include "thread_config.h"
#include "MC3172.h"
//#define THREAD_FREQCFG_VALUE       *(int*)(THD_TS_ADDR+THREAD_ID*8)//读取线程分频信息


void delay_us(u32 nus);
void delay_ms(u32 nms);
#endif /* USER_CODE_COMPONENTS_DELAY_DELAY_H_ */
