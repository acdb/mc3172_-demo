/*
 * delay.c
 *
 *  Created on: 2022年7月26日
 *      Author: w1619
 */

#include "MC3172.h"
#include "delay.h"

void delay_ms(u32 nms){
    delay_us(nms*1000);
}


inline void delay_us(u32 nus)
{
    u32 ticks;
    u32 told,tnow,tcnt=0;
    GET_CORE_CNT(told);                             //读取内核定时器数值
//    if(nus<=1){
//        NOP();
//        NOP();
//        return;
//    }
    nus-=1;
    ticks=nus*50;                                   //目标延时节拍数=需要延时时间(us)*CORE_CLK(MHz)/4
    while(1)
    {
        GET_CORE_CNT(tnow);
        if(tnow!=told)
        {
            if(tnow<told)tcnt=0xFFFFFFFF-told+tnow; //CORE_CNT递增32位，计算已延时节拍数
            else tcnt=tnow-told;
            if(tcnt>=ticks)break;                   //延时完成
        }
    };
}
