/*
 * tools.c
 *
 *  Created on: 2022年7月12日
 *      Author: Memory
 */
#include "MC3172.h"
#include "tools.h"





//初始化DEBUG串口
void DEBUG_LOG_init(void){
    INTDEV_SET_CLK_RST(GPCOM4_BASE_ADDR,(INTDEV_RUN|INTDEV_IS_GROUP0|INTDEV_CLK_IS_CORECLK_DIV16));

//    GPCOM_SET_IN_PORT(GPCOM4_BASE_ADDR,(GPCOM_RXD_IS_P2));
    GPCOM_SET_OUT_PORT(GPCOM4_BASE_ADDR,( \
            GPCOM_P0_OUTPUT_DISABLE|GPCOM_P3_OUTPUT_ENABLE|GPCOM_P2_OUTPUT_DISABLE|GPCOM_P1_OUTPUT_DISABLE| \
            GPCOM_P0_IS_HIGH       |GPCOM_P3_IS_TXD       |GPCOM_P2_IS_HIGH       |GPCOM_P1_IS_HIGH \
                      ));

    GPCOM_SET_COM_MODE(GPCOM4_BASE_ADDR,GPCOM_UART_MODE);

    GPCOM_SET_COM_SPEED(GPCOM4_BASE_ADDR,12500000,115200);

    GPCOM_SET_OVERRIDE_GPIO(GPCOM4_BASE_ADDR, ( \
            GPCOM_P2_OVERRIDE_GPIO| \
            GPCOM_P3_OVERRIDE_GPIO \
                                              ));
}


