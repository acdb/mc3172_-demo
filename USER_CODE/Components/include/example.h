/*
 * example.h
 *
 *  Created on: 2022��7��12��
 *      Author: w1619
 */

#ifndef USER_CODE_INCLUDE_EXAMPLE_H_
#define USER_CODE_INCLUDE_EXAMPLE_H_
#include "MC3172.h"
extern u8 rx_data,temp[1],humi[1],test;
void GPCOM_UART_EXAMPLE(u32 gpcom_sel);
void GPIO_EXAMPLE(u32 gpio_sel);
void pwm_test(u32 gpio_sel);
#endif /* USER_CODE_INCLUDE_EXAMPLE_H_ */
