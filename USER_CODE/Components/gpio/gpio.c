/*
 * gpio.c
 *
 *  Created on: 2022年7月26日
 *      Author: w1619
 */

#include "gpio.h"


//GPIO引脚模式变更
void gpio_pin_mode(u32 gpio_sel,u32 gpio_pin,u8 mode){
//    INTDEV_SET_CLK_RST(gpio_sel,(INTDEV_RUN|INTDEV_IS_GROUP0|INTDEV_CLK_IS_CORECLK_DIV2));
    if (mode == 1) {
        GPIO_SET_INPUT_EN_VALUE(gpio_sel,gpio_pin,GPIO_SET_DISABLE);
        GPIO_SET_OUTPUT_EN_VALUE(gpio_sel,gpio_pin,GPIO_SET_ENABLE);
    }
    if (mode == 0) {
        GPIO_SET_OUTPUT_EN_VALUE(gpio_sel,gpio_pin,GPIO_SET_DISABLE);
        GPIO_SET_INPUT_EN_VALUE(gpio_sel,gpio_pin,GPIO_SET_ENABLE);
    }
}


//读取引脚电平数据
u16 PGin(u32 gpio_sel,u32 gpio_pin){
    u16 data = 0;
    if ((GPIO_GET_INPUT_VALUE_SAFE(gpio_sel) & (gpio_pin>>16)) != (u32)GPIO_PIN_RESET){
        data = GPIO_PIN_SET;
    }
    else{
        data = GPIO_PIN_RESET;
    }
    return data;
}

