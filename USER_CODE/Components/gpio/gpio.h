/*
 * gpio.h
 *
 *  Created on: 2022��7��26��
 *      Author: w1619
 */

#ifndef USER_CODE_COMPONENTS_GPIO_GPIO_H_
#define USER_CODE_COMPONENTS_GPIO_GPIO_H_

#include "MC3172.h"


#define GPIO_PIN_IN  0
#define GPIO_PIN_OUT 1

typedef enum
{
    GPIO_PIN_RESET = 0u,
    GPIO_PIN_SET
} GPIO_PinState;

void gpio_pin_mode(u32 gpio_sel,u32 gpio_pin,u8 mode);
u16 PGin(u32 gpio_sel,u32 gpio_pin);

#endif /* USER_CODE_COMPONENTS_GPIO_GPIO_H_ */
